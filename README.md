
# LetsEnhance image Parser 

This **side-project** was created by my interest in "What people are enhancing" and also gathering base of account which can be used in future.

The algorithm of the project is:

- 1.Gather all emails from result file to set
- 2.Get the email from smailpro.com
- 3.Send a recovery letter to the email
- 4.WAit for the response 5 seconds (here few refresh clicks)
- 5.If letter is pressent, open the letter and walk via link to change the password
- 6.Login into system
- 7.Close the 'possible' notification
- 8.Scan the images and available numbers.
- 9.Save all the data and reboot the system.

For the first executions of the program,
we gonna gather only clean links which is not very usefull,
buy previously when there was an another set of emails on the site - I've gather and saved database with available emails for the logining and enhancing of the photos.
## FAQ

#### How can I use it?
You should just execute `main.py`, if there any problems read next

#### The program didn't starts, what can be the problem?
It can be changed structure of the ad on the smailpro.com - you need to check xPath at the `get_mail_value` and `refresh_mailbox` functions

#### What if I need some information about it?

You can reach me via private messages at GitLab :)

