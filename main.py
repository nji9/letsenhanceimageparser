import re
import csv
from selenium import webdriver
from time import sleep
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys

PASS = "QWE1"

def get_start_file(temp_file):
    try:
        with open(temp_file, "r", newline="", encoding="utf-8") as theFile:
            reader = csv.reader(theFile, delimiter='|')
            header = next(reader)
            return [row[1] for row in reader]
    except FileNotFoundError:
        return []


def get_mail_value(the_driver):
    """ Receive mail value from block on website """
    # block = (By.XPATH, '//*[@id="app"]/main/div[2]/div[2]/div[1]/div[1]/input')
    # quick check point

    block = (By.XPATH, '//*[@id="app"]/main/div[2]/div/div[1]/div[2]/div')

    try:
        WebDriverWait(the_driver, 6).until(EC.visibility_of_element_located(block))
        temp_mail_value = the_driver.find_element(*block).text.strip()

        for _ in range(3):
            if temp_mail_value == 'example@sonjj.com':
                sleep(1)
            temp_mail_value = the_driver.find_element(*block).text.strip()

        return temp_mail_value

    except TimeoutException as oops:
        print("--there is no MAIL--")
        raise ZeroDivisionError


def check_mail(temp_mail_value, check_here):
    return temp_mail_value in check_here


def create_new_tab(url):
    driver.execute_script(f'''window.open("{url}", "_blank");''')


def switch_window(the_driver, main_id):
    if the_driver.current_window_handle == main_id:
        arr_not_main = [win for win in driver.window_handles if win != main_id]
        the_driver.switch_to.window(arr_not_main[0])
    else:
        the_driver.switch_to.window(main_id)


def recovery_macros(the_driver, temp_mail_value):
    """ Send the_mail to the recovery input and click """
    input_block = (By.TAG_NAME, 'input')
    button_block = (By.TAG_NAME, 'button')

    try:
        WebDriverWait(the_driver, 5).until(
            EC.visibility_of_element_located(button_block))

        driver.find_element(*input_block).send_keys(temp_mail_value)
        driver.find_element(*button_block).click()

    except TimeoutException as oops:
        print("--There is no RECOVERY IMP--")


def refresh_mailbox(the_driver):
    # refresh_button = the_driver.find_element(By.XPATH, '//*[@id="app"]/main/div[2]/div[2]/div[2]/div[1]/button')
    sleep(0.5)
    # quick check point
    refresh_button = the_driver.find_element(By.XPATH, '//*[@id="app"]/main/div[3]/div[2]/div/div[1]/button')
    print(refresh_button.get_attribute('class'))

    refresh_button.click()
    sleep(2)
    refresh_button.click()
    refresh_button.click()


def letter_gather_click(the_driver):
    """ Gather one letter from mailbox """
    # sel = "px-2 py-1 cursor-pointer  hover:text-green-900 hover:bg-gray-50 bg-white my-2 rounded"
    look_block = (By.XPATH, '//*[@id="app"]/main/div[3]/div[2]/div/div[2]/div[1]/div')

    try:
        print("Stop... Wait a minute")
        # WebDriverWait(the_driver, 7).until(EC.text_to_be_present_in_element_attribute(look_block, "class", sel))
        WebDriverWait(the_driver, 5).until(EC.visibility_of_element_located(look_block))
        the_driver.find_element(*look_block).click()

    except TimeoutException as oops:
        print("--There is NO RECOVERY LETTER--")
        raise ZeroDivisionError


def recovery_url_gather(the_driver):
    """ Switch to frame of letter and gather recovery url """
    frame_block = (By.TAG_NAME, 'iframe')
    # prev 'bg-olive-50.w-full.rounded-lg.shadow'

    # letter_div = 'p-2.flex-auto.flex.flex-col'
    letter_div = '//*[@id="app"]/main/div[3]/div[2]/div/div[2]'

    letter_frame = the_driver.find_element(By.XPATH, letter_div)

    try:
        WebDriverWait(letter_frame, 6).until(EC.visibility_of_element_located(frame_block))

        letter_frame = letter_frame.find_element(*frame_block)

        letter_frame.screenshot("_test_frame.png")

        driver.switch_to.frame(letter_frame)

        recovery_letter_button = driver.find_element(By.TAG_NAME, 'a')
        temp_rec_url = recovery_letter_button.get_attribute('href')

        driver.switch_to.default_content()

        return temp_rec_url

    except TimeoutException as oops:
        print("--ERROR OF URL IN FRAME--")


def fill_in_pass(the_driver):

    inp_block = (By.TAG_NAME, 'input')

    try:
        WebDriverWait(the_driver, 3).until(EC.visibility_of_element_located(inp_block))

        olymp = the_driver.find_elements(By.TAG_NAME, 'input')
        for inp in olymp:
            inp.send_keys(PASS)
        the_driver.find_element(By.TAG_NAME, 'button').click()
        # recovering pass by entering the NEW one

    except TimeoutException as oops:
        print("--fill pass failed--")


def login_into(the_driver, temp_mail_value):

    inp_block = (By.TAG_NAME, 'input')

    try:
        WebDriverWait(the_driver, 4).until(EC.visibility_of_element_located(inp_block))

        olymp = the_driver.find_elements(By.TAG_NAME, 'input')
        olymp[0].send_keys(temp_mail_value)
        olymp[1].send_keys(PASS)

        button_arr = the_driver.find_elements(By.TAG_NAME, 'button')
        button_arr[-1].click()

    except TimeoutException as oops:
        print("--long enter--")


def some_notif(the_driver):
    temp_path = '//*[@id="gatsby-focus-wrapper"]/div/main/div/div/div[5]/dialog/div/header/button'
    notif_block = (By.XPATH, temp_path)
    try:
        WebDriverWait(the_driver, 1.5).until(EC.visibility_of_element_located(notif_block))
        the_driver.find_element(*notif_block).click()
    except (NoSuchElementException, TimeoutException):
        print("THERE IS NO ALERT")
    # Try to close notification


def get_image_block(the_driver):
    img_block = (By.CLASS_NAME, 'ProcessedImages-module--ProcessedImagesSection--27_Gw')

    try:
        WebDriverWait(the_driver, 3).until(EC.visibility_of_element_located(img_block))

        image_block = the_driver.find_element(*img_block)
        images = image_block.find_elements(By.CLASS_NAME, 'ProcessImage-module--OverlayHiddenLink--1tb0_')

        return images

    except TimeoutException as oops:
        print("THERE IS NO ENTER OR IMG")
        raise ZeroDivisionError


def get_available_num(the_driver):
    nav_block = (By.TAG_NAME, 'nav')
    num_block = (By.TAG_NAME, 'a')
    sel = 'HeaderPlatform-module--NavLink--3exm2 HeaderPlatform-module--NavImagesButton--2lGq4'

    try:
        WebDriverWait(the_driver, 1).until(EC.visibility_of_element_located(nav_block))
        navigation_block = the_driver.find_element(*nav_block)
        WebDriverWait(navigation_block, 3).until(EC.text_to_be_present_in_element_attribute(num_block, "class", sel))

        credits_block = navigation_block.find_element(*num_block)
        credits_left = credits_block.text[0]

        return credits_left

    except TimeoutException as oops:

        print("THERE IS NO ENTER")
        return '0'


def report_ss(temp_file, temp_result):
    with open(temp_file, "a", newline='') as theFile:
        the_writer = csv.writer(theFile, delimiter='|')
        the_writer.writerow(temp_result)


def reboot(the_driver):
    """ close all tabs and clear the cache with cookies"""
    sleep(1)
    the_driver.switch_to.window(main_tab_id)
    too = len(the_driver.window_handles)
    if too > 1:
        for i in range(too-1, 0, -1):
            the_driver.switch_to.window(the_driver.window_handles[i])
            the_driver.close()
    the_driver.switch_to.window(main_tab_id)
    the_driver.delete_all_cookies()
    the_driver.get('chrome://settings/clearBrowserData')
    sleep(1)
    the_driver.execute_script(
        '''document.querySelector("body > settings-ui").shadowRoot.querySelector("#main").shadowRoot.querySelector(
        "settings-basic-page").shadowRoot.querySelector(
        "#basicPage > settings-section:nth-child(9) > settings-privacy-page").shadowRoot.querySelector(
        "settings-clear-browsing-data-dialog").shadowRoot.querySelector("#clearBrowsingDataConfirm").click();''')
    sleep(1)

if __name__ == '__main__':

    result_file = "_result.csv"
    ref = get_start_file(result_file)
    ss_num = len(ref)
    ref = set(ref)
    reg = re.compile(r"^(.*?)[\d.]")

    mail_name, answer = "", []
    driver = webdriver.Chrome()

    driver.set_window_position(-1500, 0)
    driver.maximize_window()

    while True:
        try:
            driver.get('https://smailpro.com/advanced?ver=2')
            main_tab_id = driver.current_window_handle

            the_mail = get_mail_value(driver)
            mail_name = reg.search(the_mail).group(1)

            print("Get mail success "+the_mail)

            if check_mail(mail_name, ref):
                reboot(driver)
                continue

            create_new_tab("https://letsenhance.io/restore-password")
            switch_window(driver, main_tab_id)

            recovery_macros(driver, the_mail)
            switch_window(driver, main_tab_id)

            # report if there is no letter
            answer = [str(ss_num), mail_name, the_mail, "0", "5", "XC"]

            html = driver.find_element(By.TAG_NAME, 'html')
            html.send_keys(Keys.END)

            refresh_mailbox(driver)
            letter_gather_click(driver)

            rec_url = recovery_url_gather(driver)
            switch_window(driver, main_tab_id)

            driver.get(rec_url)
            fill_in_pass(driver)

            driver.get('https://letsenhance.io/login')
            login_into(driver, the_mail)
            some_notif(driver)

            login_access = "Y"
            answer = [str(ss_num), mail_name, the_mail, "0", "0", "N"]
            # report if there is no login
            all_images = get_image_block(driver)
            available_num = get_available_num(driver)

            all_img_urls = []
            answer = [str(ss_num), mail_name, the_mail, len(all_images), available_num, login_access]
            # general clean report if evry OK
            for img in all_images:
                all_img_urls.append(img.get_attribute("href"))

            with open("_urls.txt", "a") as theFile:
                theFile.write(str(ss_num)+"|"+the_mail+"\n")
                for img in all_img_urls:
                    theFile.write(img+"\n")

            report_ss(result_file, answer)

            sleep(2)
            driver.save_screenshot("session/"+str(ss_num)+".png")
            ref.add(mail_name)
            ss_num += 1

            reboot(driver)
        except ZeroDivisionError:
            print("Ooops...")

            if answer:
                report_ss(result_file, answer)
                ref.add(mail_name)
                ss_num += 1
            reboot(driver)
